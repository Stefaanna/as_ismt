package com.company;

public class Circle extends Shape {
    public float radius;
    public int center;

    Point[] points;

    public double area(float radius) {
        return Math.PI * Math.pow(radius, 2);
    }

    public void circum() {

    }

    public void setCenter(int center) {
        this.center = center;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }
}
