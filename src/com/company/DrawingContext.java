package com.company;

public class DrawingContext {

    Point point;
    double verticalSize;
    double horizontalSize;

    public void setPoint(Point point) {
        this.point = point;
    }

    public void clearScreen() {

    }

    public double getVerticalSize() {
        return verticalSize;
    }

    public double getHorizontalSize() {
        return horizontalSize;
    }
}
