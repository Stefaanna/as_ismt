package com.company;

public abstract class Shape {
    public void draw() {
        System.out.println("Draw shape");
    }
    public void erase() {
        System.out.println("Erase shape");
    }
    public void move() {
        System.out.println("Move shape");
    }
    public void resize() {
        System.out.println("Resize shape");
    }
}
